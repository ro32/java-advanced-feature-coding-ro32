package com.sda.movies;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

public class MovieRepository {
    private static final String COMMA = ",";
    private static final Path PATH = Paths.get("src/main/resources/movies.txt");


    public List<Movie> getMovies() throws IOException {
        List<Movie> movies = new ArrayList<>();
        for (String line : Files.readAllLines(PATH)) {
            movies.add(parseFileLine(line));
        }
        return movies;
    }

    public void addMovie(Movie movie) throws IOException {
        Files.writeString(PATH, parseFileLine(movie), StandardOpenOption.APPEND);
    }

    private Movie parseFileLine(String line) {
        String[] data = line.split(COMMA);
        return new Movie(data[0], data[1], data[2], Integer.parseInt(data[3]));
    }

    private String parseFileLine(Movie movie) {
        return movie.getTitle() + COMMA +
                movie.getDirector() + COMMA +
                movie.getGenre() + COMMA +
                movie.getReleaseYear() + "\n";
    }

    private String parseFileLineV2(Movie movie) {
        return new StringBuilder()
                .append(movie.getTitle())
                .append(COMMA)
                .append(movie.getDirector())
                .append(COMMA)
                .append(movie.getGenre())
                .append(COMMA)
                .append(movie.getReleaseYear())
                .append("\n")
                .toString();
    }
}
