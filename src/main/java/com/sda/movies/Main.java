package com.sda.movies;

import com.github.javafaker.Faker;

import java.io.IOException;

/**
 * Create a program which will provide following features based on the Movie class objects:
 * adding objects,
 * returning object lists.
 * The Movie class should contain fields: title, genre, director, releaseYear.
 * <p>
 * Adding objects should be written to a file.
 * Displaying object list should read the text file to convert individual lines to Movie objects.
 */
public class Main {
    private static Faker faker = new Faker();

    public static void main(String[] args) throws IOException, InterruptedException {
        MovieRepository repository = new MovieRepository();

        for (int i = 1; i <= 10; i++) {
            repository.addMovie(new Movie(faker.book().title(), faker.book().author(), faker.book().genre(), faker.number().numberBetween(1800, 2020)));
        }

        repository.getMovies().forEach(System.out::println);
    }
}
