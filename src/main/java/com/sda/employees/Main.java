package com.sda.employees;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 1. Create Employee class with id, name and salary fields. Create necessary methods and constructor.
 * Create 3 instances of the Employee class and add them to a list.
 * Using streams, find first Employee that is not null and has salary of at least 30000.
 * If not found, throw exception.
 * 3. Based on employees list from exercise1, using streams get a number representing maximum salary of all employees.
 */

public class Main {

    private static List<Employee> employees = new ArrayList<>();

    public static void main(String[] args) {
        Employee employee1 = new Employee("Ionel", 1234, 8000);
        Employee employee2 = new Employee("Gigel", 1235, 2500.0);
        Employee employee3 = new Employee("Gheorghita", 1236, 3000.0);

        employees.add(employee1);
        employees.add(employee2);
        employees.add(employee3);

        System.out.println(findBySalary(3000));
        System.out.println(sortByName());
        System.out.println(maxSalary());

    }

    // Task 1
    public static Employee findBySalary(double salaryToFind) {
        return employees.stream()
                .filter(Objects::nonNull)
                .filter(employee -> employee.getSalary() >= salaryToFind)
                .findFirst()
                .orElseThrow(() -> new EmployeeNotFoundException("No employee found with salary above " + salaryToFind));
    }

    //Task 2
    public static List<Employee> sortByName() {
        return employees.stream()
                .sorted(Comparator.comparing(Employee::getName))
                .collect(Collectors.toList());
    }

    //Task 3
    public static double maxSalary() {
        return employees.stream()
                .filter(Objects::nonNull)
                .map(Employee::getSalary)
                .max(Double::compare)
                .orElseThrow(() -> new EmployeeNotFoundException("We have no employee to compare salary"));
    }

    //Task 3.1
    public static double maxSalary1() {
        return employees.stream()
                .filter(Objects::nonNull)
                .mapToDouble(Employee::getSalary)
                .max()
                .getAsDouble();
    }
}
