package com.sda.employees.v2;

import java.util.*;
import java.util.stream.Collectors;

public class WorkWithEmployee {
    private List<Employee> employees;

    public WorkWithEmployee() {
        this.employees = new ArrayList<>();
    }

    public void addEmployee(Employee employee) {
        this.employees.add(employee);
    }

    public Employee checkEmployee(double salary) {
        return employees.stream()
                .filter(employee -> employee != null && employee.getSalary() >= salary)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("No such salary exists!"));
    }

    public List<Employee> getNameList() {
        return employees.stream()
                .sorted(Comparator.comparing(Employee::getName))
                .collect(Collectors.toList());
    }

    public Double showTheBiggestSalary(){
        return employees.stream()
                .map(Employee::getSalary)
                .max(Double::compare)
                .get();
    }
}