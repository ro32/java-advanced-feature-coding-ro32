package com.sda.employees.v2;

//Create Employee class with id, name and salary fields. Create necessary methods and constructor.
// Create 3 instances of the Employee class and add them to a list.
// Using streams, find first Employee that is not null and has salary of at least 30000. If not found, throw exception.
//Based on employees list from previous exercise, using streams create new list with employees sorted by name.
//Based on employees list from exercise1, using streams get a number representing maximum salary of all employees.

public class Main {

    public static void main(String[] args) throws Exception {
        WorkWithEmployee workWithEmployee = new WorkWithEmployee();
        Employee employee1 = new Employee(1, "Ana", 1200);
        Employee employee2 = new Employee(2, "Alin", 3500);
        Employee employee3 = new Employee(3, "Marius", 1800);
        workWithEmployee.addEmployee(employee1);
        workWithEmployee.addEmployee(employee2);
        workWithEmployee.addEmployee(employee3);
        System.out.println(workWithEmployee.checkEmployee(3000));
        System.out.println(workWithEmployee.getNameList());
        System.out.println(workWithEmployee.showTheBiggestSalary());
    }
}
