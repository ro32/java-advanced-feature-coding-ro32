package com.sda.carservice;

public class Car {
    private String engine;
    private int year;
    private double price;
    private String manufacturer;

    public Car(String engine, int year, double price, String manufacturer) {
        this.engine = engine;
        this.year = year;
        this.price = price;
        this.manufacturer = manufacturer;
    }

    public String getEngine() {
        return engine;
    }

    public int getYear() {
        return year;
    }

    public double getPrice() {
        return price;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    @Override
    public String toString() {
        return "Car{" +
                "engine='" + engine + '\'' +
                ", year=" + year +
                ", price=" + price +
                ", manufacturer='" + manufacturer + '\'' +
                '}';
    }
    
    
}
