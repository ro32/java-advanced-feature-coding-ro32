package com.sda.carservice;

import java.util.*;
import java.util.stream.Collectors;

public class CarService {
    private List<Car> cars;

    public CarService() {
        this.cars = new ArrayList<>();
    }

    public void addCar(Car car) {
        this.cars.add(car);
    }

    public void addCars(List<Car> cars) {
        this.cars.addAll(cars);
    }

    public void removeCar(Car car) {
        this.cars.remove(car);
    }

    public List<Car> getCars() {
        return this.cars;
    }

    public List<Car> getCarsBeforeYear(int year) {
        List<Car> temporaryList = new ArrayList<>();
        for (Car carTempList : cars) {
            if (carTempList.getYear() <= year) {
                temporaryList.add(carTempList);
            }
        }
        return temporaryList;
    }

    public List<Car> findByEngine(String newEngine) {
        List<Car> carsList = new ArrayList<>();
        for (Car car : this.cars) {
            if (car.getEngine().equalsIgnoreCase(newEngine)) {
                carsList.add(car);
            }
        }
        return carsList;
    }

    public Car showTheMostExpensiveCar(){
        Car car = null;
        for (Car value : cars) {
            if (car == null || car.getPrice() < value.getPrice()) {
                car = value;
            }
        }
        return car;
    }

//    returning a list of all cars sorted according to the passed parameter: ascending / descending,
    public List<Car> sortByYear(boolean isAscending) {
        return isAscending ?
                cars.stream()
                        .sorted(Comparator.comparing(Car::getYear))
                        .collect(Collectors.toList()) :
                cars.stream()
                        .sorted(Comparator.comparing(Car::getYear).reversed())
                        .collect(Collectors.toList());
    }

    public List<Car> getManufacturerList(String manufacturer) {
        List<Car> carList = new ArrayList<>();
        for (Car car : cars) {
            if (car.getManufacturer().equals(manufacturer)) {
                carList.add(car);
            }
        }
        return carList;
    }

    public Car showTheCheapestCar() {
        Car car = null;
        for (Car value : cars) {
            if (car == null || car.getPrice() > value.getPrice()) {
                car = value;
            }
        }
        return car;
    }

    public List<Car> findByBrandAndYear(String newBrand, int newYear) {
        List<Car> carsList = new ArrayList<>();
        for (Car car : this.cars) {
            if (car.getManufacturer().equalsIgnoreCase(newBrand) && car.getYear() < newYear) {
                carsList.add(car);
            }
        }
        return carsList;
    }


    public boolean checkCar(String manufacturer) {
        for (Car car : cars) {
            if (car.getManufacturer().equalsIgnoreCase(manufacturer)) {
                return true;
            }
        }
        return false;
    }

    public boolean checkCar1(String manufacturer) {
        return cars.stream()
                .map(Car::getManufacturer)
                .anyMatch(m->m.equalsIgnoreCase(manufacturer));
    }

}
