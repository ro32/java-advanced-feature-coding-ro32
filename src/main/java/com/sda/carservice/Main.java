package com.sda.carservice;

/**
 * Create a CarService class that will contain a list of cars and implement the following methods:
 * 1.adding cars to the list,
 * 2.removing a car from the list,
 * 3.returning a list of all cars,
 * 4.returning cars with a V12 engine,
 * 5.returning cars produced before 1999,
 * 6.returning the most expensive car,
 * 7.returning the cheapest car,
 * 8.returning the carList with brand,
 * 9.returning a list of all cars sorted according to the passed parameter: ascending / descending,
 * 10.checking if a specific car is on the list,
 * 11.returning a list of cars manufactured by a specific manufacturer,
 * 12.returning the list of cars manufactured by the manufacturer with the year of establishment <,>, <=,> =, =,!= from the given.
 */
public class Main {
    public static void main(String[] args) {
        CarService carService = new CarService();
        Car car1 = new Car("V12", 2000, 5000.0, "Skoda");
        Car car2 = new Car("V10", 2010, 8000.0, "Opel");
        Car car3 = new Car("V8", 2015, 10000.0, "Opel");
        Car car4 = new Car("V10", 1998, 9000.0, "Ford");
        Car car5 = new Car("V99", 1997, 4000.0, "Tesla");
        carService.addCar(car1);
        carService.addCar(car2);
        carService.addCar(car3);
        carService.addCar(car4);
        carService.addCar(car5);

        System.out.println(carService.getCarsBeforeYear(1999));

        System.out.println(carService.findByEngine("V12"));
        System.out.println(carService.sortByYear(false));
        System.out.println(carService.getManufacturerList("Opel"));
        System.out.println(carService.showTheMostExpensiveCar());
        System.out.println(carService.showTheCheapestCar());
        System.out.println(carService.findByBrandAndYear("Opel",2020));
        System.out.println(carService.checkCar("Opel"));
        System.out.println(carService.checkCar1("Tesla"));
    }
}
