package com.sda.listoperations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Creati doua liste cu urmatoarele valori:
 * ["John", "Sarah", "Mark", "Tyla", "Ellisha", "Eamonn"]
 * [1, 4, 2346, 123, 76, 11, 0, 0, 62, 23, 50]
 * Realizati urmatoarele operatii:
 * a. sortati listele (alfabetic, crescator)
 * b. afisati doar acele nume care incep cu litera "E"
 * c. afisati valorile mai mari decat 30 si mai mici decat 200
 * d. afisati numele cu litera mare (uppercase)
 * e. stergeti prima si ultima litera, sortati si afisati numele
 */
public class MainV2 {
    public static void main(String[] args) {

        List<String> name = new ArrayList<>(Arrays.asList("John", "Sarah", "Mark", "Tyla", "Ellisha", "Eamonn"));
        List<Integer> number = new ArrayList<>(Arrays.asList(1, 4, 2346, 123, 76, 11, 0, 0, 62, 23, 50));

        //a.
        alphabeticallyOrder(name);
        System.out.println("==============");
        sortedAscending(number);
        System.out.println("==============");

        //b
        nameWithLetterE(name);
        System.out.println("==============");
        numberBetwen(number);
        System.out.println("==============");

        //d.
        nameWithUppercase(name);

        //e.
        System.out.println("===========");
        deleteLetterSortedAndDisplay(name);
    }

    private static void alphabeticallyOrder(List<String> name) {
        System.out.println("Sorted in alphabetically order");
        name.stream().sorted().toList().forEach(System.out::println);
    }

    private static void sortedAscending(List<Integer> number) {
        System.out.println("Sorted in ascending order");
        Collections.sort(number);
        System.out.println(number);
    }

    private static void nameWithLetterE(List<String> name) {
        System.out.println("Name starts with E letter");
        for (String names : name) {
            if (names.startsWith("E")) {
                System.out.println(names);
            }
        }
    }

    private static void numberBetwen(List<Integer> number) {
        System.out.println("Numbers betwen 30 <=> 200");
        for (Integer integers : number) {
            if (integers > 30 && integers < 200) {
                System.out.println(integers);
            }
        }

    }

    private static void nameWithUppercase(List<String> name) {
        System.out.println("All name with uppercase");
        name.stream().map(String::toUpperCase).forEach(names -> System.out.println(names + " "));
    }

    private static void deleteLetterSortedAndDisplay(List<String> name) {
        System.out.println();
        System.out.println("Deleted first and last char, sorted and displaying");
        List<String> currentName = new ArrayList<>();
        for (String names : name) {
            String currentName2 = names.substring(1, names.length() - 1);
            System.out.println(currentName2);
            currentName.add(currentName2);
        }
        System.out.println("========");
        System.out.println("Sorted after deleting the first and last letter");
        Collections.sort(currentName);
        System.out.println(currentName);
    }
}
