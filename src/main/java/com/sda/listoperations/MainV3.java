package com.sda.listoperations;

import java.util.ArrayList;
import java.util.List;

/**
 * Creati doua liste cu urmatoarele valori:
 * ["John", "Sarah", "Mark", "Tyla", "Ellisha", "Eamonn"]
 * [1, 4, 2346, 123, 76, 11, 0, 0, 62, 23, 50]
 * Realizati urmatoarele operatii:
 * a. sortati listele (alfabetic, crescator)
 * b. afisati doar acele nume care incep cu litera "E"
 * c. afisati valorile mai mari decat 30 si mai mici decat 200
 * d. afisati numele cu litera mare (uppercase)
 * e. stergeti prima si ultima litera, sortati si afisati numele
 */
public class MainV3 {
    public static void main(String[] args) {
        List<String> names = new ArrayList<>();
        names.add("John");
        names.add("Sarah");
        names.add("Mark");
        names.add("Tyla");
        names.add("Ellisha");
        names.add("Eamonn");

        List<Integer> numbers = new ArrayList<>();
        numbers.add(1);
        numbers.add(4);
        numbers.add(2346);
        numbers.add(123);
        numbers.add(76);
        numbers.add(11);
        numbers.add(0);
        numbers.add(0);
        numbers.add(62);
        numbers.add(23);
        numbers.add(50);

        System.out.println("Names sorted alphabetically: " + names.stream().sorted().toList());
        System.out.println("Numbers sorted ascending: " + numbers.stream().sorted().toList());
        System.out.println("Numbers between 30 & 200: " + numbers.stream().filter(integer -> integer > 30).filter(integer -> integer < 200).toList());
        System.out.println("Names which contains E: " + names.stream().filter(s -> s.contains("E")).toList());
        System.out.println("Names with upperCase: " + names.stream().map(String::toUpperCase).toList());
        System.out.println(names.stream().map(str -> str.substring(1, str.length() - 1)).toList());
    }

}
