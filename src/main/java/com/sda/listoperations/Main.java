package com.sda.listoperations;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Creati doua liste cu urmatoarele valori:
 * ["John", "Sarah", "Mark", "Tyla", "Ellisha", "Eamonn"]
 * [1, 4, 2346, 123, 76, 11, 0, 0, 62, 23, 50]
 * Realizati urmatoarele operatii:
 * a. sortati listele (alfabetic, crescator)
 * b. afisati doar acele nume care incep cu litera "E"
 * c. afisati valorile mai mari decat 30 si mai mici decat 200
 * d. afisati numele cu litera mare (uppercase)
 * e. stergeti prima si ultima litera, sortati si afisati numele
 */
public class Main {
    public static void main(String[] args) {
        List<String> names = new ArrayList<>();
        names.add("John");
        names.add("Sarah");
        names.add("Mark");
        names.add("Tyla");
        names.add("Ellisha");
        names.add("Eamonn");

        List<Integer> integers = new ArrayList<>();
        integers.add(1);
        integers.add(4);
        integers.add(2346);
        integers.add(123);
        integers.add(76);
        integers.add(11);
        integers.add(0);
        integers.add(0);
        integers.add(62);
        integers.add(23);
        integers.add(50);

//        a. sort
        List<String> sortedNames = names.stream()
                .sorted()
                .collect(Collectors.toList());
        System.out.println(sortedNames);

        List<Integer> sortedInts = integers.stream()
                .sorted()
                .collect(Collectors.toList());
        System.out.println(sortedInts);

//        b. names starting with 'E'

        List<String> namesWithE = names.stream().
                filter(name -> name.startsWith("E"))
                .collect(Collectors.toList());
        System.out.println(namesWithE);

//        c. integers between 30 and 200

        List<Integer> integers30200 = integers.stream()
                .filter(integer -> integer > 30 && integer < 200)
                .collect(Collectors.toList());
        System.out.println(integers30200);

//        d. uppercase names

        List<String> upperCaseNames = names.stream()
                .map(String::toUpperCase)
                .collect(Collectors.toList());
        System.out.println(upperCaseNames);

//        e. erase first and last letter, sort and display

        List<String> substringNames = names.stream()
                .map(name -> name.substring(0, name.length()-1))
                .sorted()
                .collect(Collectors.toList());
        System.out.println(substringNames);
    }
}
