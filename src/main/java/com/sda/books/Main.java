package com.sda.books;

/**
 * Ex 2
 * Create a Author class that will contain:
 * Three private instance variables: name (String), email (String), and gender (char of either 'm' or 'f');
 * One constructor to initialize the name, email and gender with the given values;
 * (There is no default constructor for Author, as there are no defaults for name, email and gender.)
 * public getters/setters: getName(), getEmail(), setEmail(), and getGender();
 * (There are no setters for name and gender, as these attributes cannot be changed.)
 * A toString() method that returns "author-name (gender) at email", e.g., "Tan Ah Teck (m) at ahTeck@somewhere.com"
 * Also write a test program called TestAuthor to test the constructor and public methods. Try changing the email of an author, e.g.,
 * Now, create a class called Book that will contain:
 * Four private instance variables: name (String), author (of the class Author you have just created, assume that each book has one and only one author), price (double), and qtyInStock (int);
 * Two constructors:
 * One will contain: name, author and price fields
 * One will contain name, author, price, qtyInStock fields
 * public methods getName(), getAuthor(), getPrice(), setPrice(), getQtyInStock(), setQtyInStock().
 * toString() that returns "'book-name' by author-name (gender) at email".
 * (Take note that the Author's toString() method returns "author-name (gender) at email".)
 * Also write a test program called TestBook to test the constructor and public methods in the class Book. Take Note that you have to construct an instance of Author before you can construct an instance of Book.
 * TRY:
 * Printing the name and email of the author from a Book instance. (Hint: aBook.getAuthor().getName(), aBook.getAuthor().getEmail()).
 * Introduce new methods called getAuthorName(), getAuthorEmail(), getAuthorGender() in the Book class to return the name, email and gender of the author of the book.
 */
public class Main {

    public static void main(String[] args) {
        Author author = new Author("Author1", "author1@mail", Gender.M);
        Book book = new Book("Book1", 120, 1, author);

        testBook(book);
        testAuthor();
    }

    public static void testAuthor() {
        Author author = new Author("Author", "author@mail", Gender.M);
        author.setEmail("test@gmail.com");
        System.out.println("New Author: " + author);

        Author author1 = new Author("Author1", "author1mail", Gender.F);
        System.out.println("New Author: " + author);
    }

    public static void testBook(Book book) {
        System.out.println(book.getAuthor());
        System.out.println(book.getName());
        System.out.println(book.getPrice());
        System.out.println(book.getQtyInStock());
    }

}
