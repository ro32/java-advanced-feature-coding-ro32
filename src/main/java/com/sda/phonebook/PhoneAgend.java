package com.sda.phonebook;

import java.util.ArrayList;
import java.util.List;

public class PhoneAgend {
    List<String> phoneNumbers;

    public PhoneAgend() {
        this.phoneNumbers = new ArrayList<>();
    }

    public void addNumber(String number) {
        if (phoneNumbers.size() < 100) {
            phoneNumbers.add(number);
        } else {
            System.out.println("Memory is full!");
        }
    }

    public boolean isPresent(String number) {
        return phoneNumbers.stream()
                .anyMatch(n -> n.equals(number));
    }

    public void printContacts() {
        phoneNumbers.forEach(System.out::println);
    }
}
