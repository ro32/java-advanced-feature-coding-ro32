package com.sda.phonebook;

/**
 * Agenda telefonica
 * Creati o agenda pentru numere de telefon de capacitate 100;
 * Putem sa adaugam un numar in agenda doar daca nu s-a atins capacitatea maxima.
 * Putem sa cautam in agenda dupa un numar de telefon sa vedem daca acesta exista sau nu.
 * Putem sa afisam toate numerele de telefon existente in agenda.
 */
public class Main {
    public static void main(String[] args) {
        PhoneAgend phoneAgend = new PhoneAgend();

        phoneAgend.addNumber("123456789");

        phoneAgend.printContacts();
        System.out.println(phoneAgend.isPresent("987654321"));
        System.out.println(phoneAgend.isPresent("123456789"));

//      testing add method
        for (int i = 1; i < 100; i++) {
            phoneAgend.addNumber("123456789");
        }
        phoneAgend.addNumber("123456789");

        System.out.println("-------- Test Phonebook ---------");

        Phonebook phoneContact = new Phonebook();

        phoneContact.addPhoneNumbers("Nume1", "012314251");
        phoneContact.addPhoneNumbers("Nume2", "012314252");
        phoneContact.addPhoneNumbers("Nume3", "012314253");
        phoneContact.addPhoneNumbers("Nume4", "012314254");
        phoneContact.addPhoneNumbers("Nume5", "012314255");

        phoneContact.findPhoneNumberByKeys("Nume3");
        System.out.println("=======================");
        phoneContact.findPhoneNumberByKeys("Nume");
        System.out.println("=======================");
        phoneContact.printPhoneNumbers();

    }
}

