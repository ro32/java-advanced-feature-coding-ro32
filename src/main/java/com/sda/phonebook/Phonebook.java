package com.sda.phonebook;

import java.util.HashMap;
import java.util.Map;

public class Phonebook {

    private Map<String, String> phoneNumbers;

    public Phonebook() {
        this.phoneNumbers = new HashMap<>();
    }

    public void addPhoneNumbers(String key, String value) {
        if (this.phoneNumbers.size() < 100) {
            this.phoneNumbers.put(key, value);
        } else {
            System.out.println("You have reached the maximum limit");
        }
    }

    public void findPhoneNumberByKeys(String keys) {
        String values = null;
        for (Map.Entry<String, String> entry : phoneNumbers.entrySet()) {
            if (entry.getKey().equalsIgnoreCase(keys)) {
                values = entry.getValue();
            }
        }
        System.out.println(values);
    }

    public void printPhoneNumbers() {
        for (Map.Entry<String, String> entry : phoneNumbers.entrySet()) {
            System.out.println(entry.getKey() + " => " + entry.getValue());
        }
    }
}
