package com.sda.vechicles;

public class Cars extends Vehicles {
    private int topSpeed;
    private Transmission transmission;
    private CarShape shape;

    public int getTopSpeed() {
        return topSpeed;
    }

    public void setTopSpeed(int topSpeed) {
        this.topSpeed = topSpeed;
    }

    public void setTransmission(Transmission transmission) {
        this.transmission = transmission;
    }

    public void setShape(CarShape shape) {
        this.shape = shape;
    }

    @Override
    public String toString() {
        return "Cars{" +
                super.toString() +
                "topSpeed=" + topSpeed +
                ", transmission=" + transmission +
                ", carShape=" + shape +
                '}';
    }
}
