package com.sda.vechicles;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;

import static com.sda.vechicles.MotorcycleShape.CHOPPER;

/**
 * TODO
 * ## Vehicle IO application requirements
 * <p>
 * Build an OOP hierarchy while considering the following types of objects:
 * <p>
 * Cars
 * Brand
 * Model
 * Price
 * TopSpeed
 * Transmission(Manual/Automatic)
 * Shape(Coupe/Sedan/Wagon)
 * <p>
 * Motorcycles
 * Brand
 * Model
 * Price
 * TopSpeed
 * Shape(Chopper/Cruiser/Enduro)
 * <p>
 * Tractors
 * Brand
 * Model
 * Price
 * MaxPulledWeight
 * <p>
 * <p>
 * * Read vehicles.txt (found under resources folder) and create objects of the proper type
 * * Count the number of cars, motorcycles, tractors
 * * Count how many vehicles of each brand are there
 * * Sort the cars by price
 * * Sort the choppers by top speed
 * * Display each category of vehicles in separate files
 */
public class Main {

    private static final String COMMA = ",";
    private static List<Cars> cars = new ArrayList<>();
    private static List<Motorcycles> motorcycles = new ArrayList<>();
    private static List<Tractors> tractors = new ArrayList<>();
    private static List<Vehicles> vehicles = new ArrayList<>();
    private static Path path = Paths.get("src/main/resources/vehicles.txt");

    public static void main(String[] args) throws IOException {
        readVehicles();
        System.out.println(cars);
        System.out.println(tractors);
        System.out.println(motorcycles);
        System.out.println(vehicles);
        countVehiclesByBrand();
        countVehiclesByType();
        sortCarByPrice();
        sortChopperBySpeed();
        writeEachVehiclesInFiles();
    }

    private static void readVehicles() throws IOException {
        List<String> lines = Files.readAllLines(path);
        for (String line : lines) {
            Vehicles vehicle = convertLineToVehicle(line);
            vehicles.add(vehicle);
        }
    }

    private static Vehicles convertLineToVehicle(String line) throws IOException {
        String[] data = line.split(COMMA);
        Vehicles vehicle = new Vehicles();
        switch (data[0]) {
            case "Car":
                vehicle = convertLineToCar(data);
                cars.add((Cars) vehicle);
                break;
            case "Motorcycle":
                vehicle = convertLineToMotorcycle(data);
                motorcycles.add((Motorcycles) vehicle);
                break;
            case "Tractor":
                vehicle = convertLineToTractor(data);
                tractors.add((Tractors) vehicle);
                break;
        }
        return vehicle;
    }

    private static Cars convertLineToCar(String[] carsConverted) {
        Cars car = new Cars();
        car.setBrand(carsConverted[1].trim());
        car.setModel(carsConverted[2].trim());
        car.setPrice(Double.parseDouble(carsConverted[3].trim()));
        car.setTopSpeed(Integer.parseInt(carsConverted[4].trim()));
        car.setTransmission(Transmission.valueOf(carsConverted[5].trim()));
        car.setShape(CarShape.valueOf(carsConverted[6].trim()));

        return car;
    }

    private static Motorcycles convertLineToMotorcycle(String[] data) {
        Motorcycles motorcycle = new Motorcycles();
        motorcycle.setBrand(data[1].trim());
        motorcycle.setModel(data[2].trim());
        motorcycle.setPrice(Double.parseDouble(data[3].trim()));
        motorcycle.setTopSpeed(Integer.parseInt(data[4].trim()));
        motorcycle.setShape(MotorcycleShape.valueOf(data[5].trim()));
        return motorcycle;
    }

    private static Tractors convertLineToTractor(String[] parseLine) {
        Tractors tractor = new Tractors();

        tractor.setBrand(parseLine[1].trim());
        tractor.setModel(parseLine[2].trim());
        try {
            tractor.setPrice(Double.parseDouble(parseLine[3].trim()));
        } catch (Exception e) {
            System.out.println("cannot parse price");
        }
        tractor.setMaxPulledWeight(Double.parseDouble(parseLine[4].trim()));

        return tractor;
    }

//    Count the number of cars, motorcycles, tractors

    private static void countVehiclesByType() {
        int countCar = 0;
        int countMotorcycle = 0;
        int countTractor = 0;

        for (Vehicles vehicle : vehicles) {
            if (vehicle instanceof Cars) {
                countCar++;
            } else if (vehicle instanceof Motorcycles) {
                countMotorcycle++;
            } else if (vehicle instanceof Tractors) {
                countTractor++;
            }
        }
        System.out.printf("Cars number: %d; %nMotorcycles number: %d; %nTractors number: %d;%n", countCar, countMotorcycle, countTractor);
    }

//   Count how many vehicles of each brand are there

    private static void countVehiclesByBrand() {
        Map<String, Integer> map = new HashMap<>();
        for (Vehicles vehicle : vehicles) {
            if (map.containsKey(vehicle.getBrand())) {
                int counter = map.get(vehicle.getBrand());
                counter++;
                map.put(vehicle.getBrand(), counter);
            } else {
                map.put(vehicle.getBrand(), 1);
            }
        }
        System.out.println(map);
    }

//    Sort the cars by price

    private static void sortCarByPrice(){
        System.out.println("Display sort car by price:");
        cars.stream().sorted(Comparator.comparing(Cars::getPrice)).forEach(System.out::println);
    }

//    Sort the choppers by top speed

    private static void sortChopperBySpeed(){
        System.out.println("Display chopper: ");
        motorcycles.stream()
                .filter(motorcycles1 -> motorcycles1.getShape().equals(CHOPPER))
                .sorted(Comparator.comparing(Motorcycles::getTopSpeed))
                .forEach(System.out::println);
    }

//    Display each category of vehicles in separate files

    private static void writeEachVehiclesInFiles() throws IOException {
        writeCars();
        writeMotorcycles();
        writeTractors();

    }

    private static void writeCars() throws IOException {
        Path pathCars = Paths.get("src/main/resources/cars.txt");
        Files.writeString(pathCars, "", StandardOpenOption.CREATE);
        for (Cars car : cars) {
            Files.writeString(pathCars, car.toString() + "\n", StandardOpenOption.APPEND);
        }
    }

    private static void writeMotorcycles() throws IOException {
        Path pathMotorcycle = Paths.get("src/main/resources/motorcycles.txt");
        Files.writeString(pathMotorcycle, "", StandardOpenOption.CREATE);
        for (Motorcycles motorcycle : motorcycles) {
            Files.writeString(pathMotorcycle, motorcycle.toString()+ "\n", StandardOpenOption.APPEND);
        }
    }

    private static void writeTractors() throws IOException {
        Path pathTractor = Paths.get("src/main/resources/tractors.txt");
        Files.writeString(pathTractor, "", StandardOpenOption.CREATE);
        for (Tractors tractor : tractors) {
            Files.writeString(pathTractor, tractor.toString() + "\n", StandardOpenOption.APPEND);
        }
    }
}
