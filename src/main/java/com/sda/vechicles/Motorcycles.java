package com.sda.vechicles;

public class Motorcycles extends Vehicles{
    private int topSpeed;
    private MotorcycleShape shape;

    public int getTopSpeed() {
        return topSpeed;
    }

    public void setTopSpeed(int topSpeed) {
        this.topSpeed = topSpeed;
    }

    public void setShape(MotorcycleShape shape) {
        this.shape = shape;
    }

    public MotorcycleShape getShape() {
        return shape;
    }

    @Override
    public String toString() {
        return "Motorcycles{" +
                super.toString() +
                "topSpeed=" + topSpeed +
                ", shape=" + shape +
                '}';
    }
}
