package com.sda.vechicles;

public class Tractors extends Vehicles {
    private double maxPulledWeight;

    public void setMaxPulledWeight(double maxPulledWeight) {
        this.maxPulledWeight = maxPulledWeight;
    }

    @Override
    public String toString() {
        return "Tractors{" +
                super.toString() +
                "maxPulledWeight=" + maxPulledWeight +
                '}';
    }
}
