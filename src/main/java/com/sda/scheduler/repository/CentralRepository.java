package com.sda.scheduler.repository;

import com.github.javafaker.Faker;
import com.sda.scheduler.exception.MaximumNumberOfStudentsReachedException;
import com.sda.scheduler.model.Group;
import com.sda.scheduler.model.Student;
import com.sda.scheduler.model.Trainer;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


public class CentralRepository {

    private static Faker faker = new Faker();

    private static List<Student> studentsRepo = new ArrayList<>();
    private static List<Trainer> trainersRepo = new ArrayList<>();
    private static List<Group> groupsRepo = new ArrayList<>();

    static {
        for (int i = 0; i < 15; i++) {
            Student student = new Student(faker.name().firstName(),
                    faker.name().lastName(),
                    LocalDate.of(faker.number().numberBetween(1985, 2005), faker.number().numberBetween(1, 12), faker.number().numberBetween(1, 28)),
                    faker.random().nextBoolean());
            studentsRepo.add(student);
        }
        for (int i = 0; i < 3; i++) {
            Trainer trainer = new Trainer(faker.name().firstName(),
                    faker.name().lastName(),
                    LocalDate.of(faker.number().numberBetween(1985, 2005), faker.number().numberBetween(1, 12), faker.number().numberBetween(1, 28)),
                    faker.random().nextBoolean());
            trainersRepo.add(trainer);
        }
        for (int i = 0; i < 4; i++) {
            Group group = new Group();
            group.setName("Group" + (i + 1));
            if (i == 3) {
                group.setTrainer(trainersRepo.get(faker.number().numberBetween(0, 2)));
            } else {
                group.setTrainer(trainersRepo.get(i));
            }
            groupsRepo.add(group);
        }
        try {
            groupsRepo.get(0).addStudent(studentsRepo.get(0));
            groupsRepo.get(0).addStudent(studentsRepo.get(1));
            groupsRepo.get(0).addStudent(studentsRepo.get(2));
            groupsRepo.get(0).addStudent(studentsRepo.get(12));


            groupsRepo.get(1).addStudent(studentsRepo.get(3));
            groupsRepo.get(1).addStudent(studentsRepo.get(4));
            groupsRepo.get(1).addStudent(studentsRepo.get(5));
            groupsRepo.get(1).addStudent(studentsRepo.get(13));

            groupsRepo.get(2).addStudent(studentsRepo.get(6));
            groupsRepo.get(2).addStudent(studentsRepo.get(7));
            groupsRepo.get(2).addStudent(studentsRepo.get(8));
            groupsRepo.get(2).addStudent(studentsRepo.get(14));

            groupsRepo.get(3).addStudent(studentsRepo.get(9));
            groupsRepo.get(3).addStudent(studentsRepo.get(10));
            groupsRepo.get(3).addStudent(studentsRepo.get(11));

//            groupsRepo.get(0).getStudents().add(studentsRepo.get(0));

        } catch (MaximumNumberOfStudentsReachedException e) {
            System.out.println("Cannot add more students");
        }
    }


    public static List<Student> getStudentsRepo() {
        return studentsRepo;
    }

    public static List<Trainer> getTrainersRepo() {
        return trainersRepo;
    }

    public static List<Group> getGroupsRepo() {
        return groupsRepo;
    }
}
