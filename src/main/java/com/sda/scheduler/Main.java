package com.sda.scheduler;

import com.sda.scheduler.model.Group;
import com.sda.scheduler.model.Student;
import com.sda.scheduler.model.Trainer;
import com.sda.scheduler.repository.CentralRepository;

import java.time.LocalDate;
import java.time.Period;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.sda.scheduler.repository.CentralRepository.getGroupsRepo;
import static com.sda.scheduler.repository.CentralRepository.getStudentsRepo;

/**
 * SDA Scheduler application requirements
 * <p>
 * Create a class hierarchy
 * <p>
 * Person.java - firstname
 * - lastname
 * - dateOfBirth
 * Trainer.java (extends Person) - isAuthorized (boolean)
 * Student.java (extends Person) - hasPreviousJavaKnowledge (boolean)
 * <p>
 * Create a Group class which has
 * <p>
 * name (Java2Gdansk, Tester3Bucharest, etc)
 * one trainer
 * a list of students
 * <p>
 * * Manually initialize 15 students; 4 groups and 3 trainers;
 * * Store all students in a list; all groups in a list; all trainers in a list;
 * * Asign a trainer to each group
 * * Asign 3-4 students to each group
 * * Ensure the fact that a group will only have distinct students (How would you do that?)
 * * Ensure the fact that a group will only have a maximum of 5 students; When you try to add a 6th one throw an MaximumNumberOfStudentsReached exception
 * * Display all students in a group sorted alphabetically by lastName
 * * Display the group with the maximum number of students
 * * Display all students younger than 25, from all groups
 * * Display all students grouped by trainer that teaches to them (eg. Trainer1 - stud1, stud3, stud4; Trainer2 - stud2, stud 10) - regardless of the group they're part of (If you were to store this information in a data structure what would you use?)
 * * Display all students with previous java knowledge
 * * Display the group with the highest number of students with no previous java knowledge
 * * Remove all the students younger than 20 from all groups
 */
public class Main {
    public static void main(String[] args) {
        displayAllGroups();
        System.out.println("----------------------");
        displayStudentsFirstNameSorted();
        System.out.println("----------------------");
        displayStudentsByFirstName();
        System.out.println("----------------------");
        displayGroupWithMaxNumberOfStudents();
        System.out.println("----------------------");
        displayStudentsUnderAge(25);
        System.out.println("----------------------");
        displayStudentsWithJavaKnowledge();
        System.out.println("----------------------");
        displayAllGroups();
        System.out.println("----------------------");
        removeStudentYoungerThenFromGrups(20);
        displayAllGroups();
        System.out.println("----------------------");
        displayStudentsByTrainer(extractTrainersStudents());
        System.out.println("----------------------");
        displayNoJavaKnowledgeGroup();
    }

    private static void displayStudentsUnderAge(int age) {
        List<Student> students = CentralRepository.getStudentsRepo().stream()
                .filter(student -> Period.between(student.getDateOfBirth(), LocalDate.now()).getYears() <= age)
                .collect(Collectors.toList());
        for (Student student : students) {
            System.out.println(student);
        }
    }

    private static void displayStudentsUnderAgeFromGroups(int age) {
        for (Group group : getGroupsRepo()) {
            for (Student student : group.getStudents()) {
                if (Period.between(student.getDateOfBirth(), LocalDate.now()).getYears() <= age) {
                    System.out.println(student);
                }
            }
        }
    }

    private static void displayStudentsUnderAgeV2(int age) {
        List<Student> students = getGroupsRepo().stream()
                .map(Group::getStudents)
                .flatMap(Collection::stream)
                .filter(student -> Period.between(student.getDateOfBirth(), LocalDate.now()).getYears() <= age)
                .collect(Collectors.toList());
        for (Student student : students) {
            System.out.println(student);
        }
    }

    private static void displayAllGroups() {
        System.out.println("Lista grupelor cu studentii si trainerii lor");
        for (Group group : getGroupsRepo()) {
            System.out.println(group);
        }
    }

    private static void displayStudentsFirstNameSorted() {
        System.out.println("Students sorted by firstname:");
        for (Group group : getGroupsRepo()) {
            System.out.println(group.getName());
            List<String> studentsName = group.getStudents().stream()
                    .map(Student::getFirstname)
                    .collect(Collectors.toList())
                    .stream()
                    .sorted()
                    .toList();
            System.out.println(studentsName);
        }
    }

    private static void displayStudentsByFirstName() {
        System.out.println("Students sorted by firstname:");
        for (Group group : getGroupsRepo()) {
            System.out.println(group.getName());
            List<Student> students = group.getStudents().stream()
                    .sorted(Comparator.comparing(Student::getFirstname))
                    .toList();
            System.out.println(students);
        }
    }

    private static void displayGroupWithMaxNumberOfStudents() {
        Group maxGroup = getGroupsRepo().get(0);
        for (Group group : getGroupsRepo()) {
            if (maxGroup.getStudents().size() < group.getStudents().size()) {
                maxGroup = group;
            }
        }
        System.out.println(maxGroup);
    }

    private static void displayStudentsWithJavaKnowledge() {
        System.out.println("Students with Java Knowledge are: ");
        for (Student student : getStudentsRepo()) {
            if (student.isHasPreviousJavaKnowledge()) {
                System.out.println(student);
            }
        }
    }

    private static void removeStudentYoungerThenFromGrups(int age) {
        for (Group group : getGroupsRepo()) {
            group.removeStudentsYoungerThen(age);
        }
    }

    private static void displayStudentsByTrainer(Map<Trainer, Set<Student>> trainersStudents) {
        for (Map.Entry<Trainer, Set<Student>> entry : trainersStudents.entrySet()) {
            System.out.println(entry.getKey() + " have the following students:");
            for (Student student : entry.getValue()) {
                System.out.println("   -> " + student);
            }
        }
    }

    private static Map<Trainer, Set<Student>> extractTrainersStudents() {
        Map<Trainer, Set<Student>> studentsByTrainer = new HashMap<>();
        Set<Student> studentsWithSameTrainer;

        for (Group group : getGroupsRepo()) {
            Trainer trainer = group.getTrainer();
            if (studentsByTrainer.containsKey(trainer)) {
                studentsWithSameTrainer = Stream.of(studentsByTrainer.get(trainer), group.getStudents())
                        .flatMap(Set::stream)
                        .collect(Collectors.toSet());
            } else {
                studentsWithSameTrainer = group.getStudents();
            }
            studentsByTrainer.put(trainer, studentsWithSameTrainer);
        }
        return studentsByTrainer;
    }

//  2. Display the group with the highest number of students with no previous java knowledge

    public static void displayNoJavaKnowledgeGroup() {
        Group noJavaKnowledgeGroup = getGroupsRepo().get(0);
        int maxNoJavaKnowledgeStudents = 0;

        for (Group group : getGroupsRepo()) {
            int studentCounter = 0;
            for (Student student : group.getStudents()) {
                if (!student.isHasPreviousJavaKnowledge()) {
                    studentCounter++;
                }
            }
            if (studentCounter > maxNoJavaKnowledgeStudents) {
                maxNoJavaKnowledgeStudents = studentCounter;
                noJavaKnowledgeGroup = group;
            }
        }
        System.out.println(noJavaKnowledgeGroup.getName());
    }

}
