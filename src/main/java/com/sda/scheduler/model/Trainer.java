package com.sda.scheduler.model;

import java.time.LocalDate;

public class Trainer extends Person {

    private boolean isAuthorized;

    public Trainer(String firstname, String lastname, LocalDate dateOfBirth, boolean isAuthorized) {
        super(firstname, lastname, dateOfBirth);
        this.isAuthorized = isAuthorized;
    }

    public boolean isAuthorized() {
        return isAuthorized;
    }

    @Override
    public String toString() {
        return "Trainer{" +
                "firstname=" + super.getFirstname() +
                "," +
                "lastname=" + super.getLastname() +
                "," +
                "dateOfBirth=" + super.getDateOfBirth() +
                "," +
                "isAuthorized=" + isAuthorized +
                '}';
    }
}
