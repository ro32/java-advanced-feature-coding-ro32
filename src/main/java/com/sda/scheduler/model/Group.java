package com.sda.scheduler.model;

import com.sda.scheduler.exception.MaximumNumberOfStudentsReachedException;

import java.time.LocalDate;
import java.time.Period;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Group {
    private String name;
    private Trainer trainer;
    private Set<Student> students = new HashSet<>();


    public Group() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Trainer getTrainer() {
        return trainer;
    }

    public void setTrainer(Trainer trainer) {
        this.trainer = trainer;
    }

    public Set<Student> getStudents() {
        return Collections.unmodifiableSet(students);
    }

    public void addStudent(Student student) {
        if (this.students.size() >= 5) {
            throw new MaximumNumberOfStudentsReachedException();
        }
        this.students.add(student);
    }

    public void removeStudentsYoungerThen(int age){
        this.students.removeIf(student -> Period.between(student.getDateOfBirth(), LocalDate.now()).getYears() <= age);
    }

    @Override
    public String toString() {
        return "Group{" +
                "name='" + name + '\'' +
                ", trainer=" + trainer +
                ", students=" + students +
                '}';
    }
}
