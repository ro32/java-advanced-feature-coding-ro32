package com.sda.scheduler.model;

import java.time.LocalDate;

public class Person {
    private String firstname;
    private String lastname;
    private LocalDate dateOfBirth;

    public Person(String firstname, String lastname, LocalDate dateOfBirth) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.dateOfBirth = dateOfBirth;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

}
