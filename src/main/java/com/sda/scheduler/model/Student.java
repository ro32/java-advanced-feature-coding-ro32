package com.sda.scheduler.model;

import java.time.LocalDate;

public class Student extends Person{

   private boolean hasPreviousJavaKnowledge;

    public Student(String firstname, String lastname, LocalDate dateOfBirth, boolean hasPreviousJavaKnowledge) {
        super(firstname, lastname, dateOfBirth);
        this.hasPreviousJavaKnowledge = hasPreviousJavaKnowledge;
    }

    public boolean isHasPreviousJavaKnowledge() {
        return hasPreviousJavaKnowledge;
    }

    @Override
    public String toString() {
        return "Student{" +
                "firstname=" + super.getFirstname() +
                "," +
                "lastname=" + super.getLastname() +
                "," +
                "dateOfBirth=" + super.getDateOfBirth() +
                "," +
                "hasPreviousJavaKnowledge=" + hasPreviousJavaKnowledge +
                '}';
    }
}
