package com.sda.printmaps;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Create a method that takes the map as a parameter, where the key is string and the value number, and then
 * prints each map element to the console in the format: Key: <k>, Value: <v>. There should be a comma at the
 * end of every line except the last, and a period at the last.
 * Example:
 * Key: Java, Value: 18,
 * Key: Python, Value: 1,
 * …
 * Key: PHP, Value: 0.
 */
public class Main {
    public static void main(String[] args) {
        Map<String, Integer> maps = new TreeMap<>();
        maps.put("Java", 18);
        maps.put("Python", 1);
        maps.put("Php", 0);
        maps.put("JavaScript ", 20);

        printMap(maps);

        Map<String, Integer> map = new HashMap<>();
        map.put("Java", 18);
        map.put("Python", 1);
        map.put("PHP", 0);
        printMapV2(map);
    }

    private static void printMap(Map<String, Integer> maps) {
        String lastKey = ((TreeMap<String, Integer>) maps).lastKey();
        for (Map.Entry<String, Integer> entry : maps.entrySet()) {
            if (!entry.getKey().equals(lastKey)) {
                System.out.println("Key: " + entry.getKey() + ", " + "Value: " + entry.getValue() + ",");
            } else {
                System.out.println("Key: " + entry.getKey() + ", " + "Value: " + entry.getValue() + ".");
            }
        }
    }

    public static void printMapV2(Map<String, Integer> map) {
        int mapElements = map.size();
        int counter = 1;
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            if (counter < mapElements) {
                System.out.println("Key: " + entry.getKey() + ", Value: " + entry.getValue() + ",");
            } else {
                System.out.println("Key: " + entry.getKey() + ", Value: " + entry.getValue() + ".");

            }
            counter++;
        }
    }
}