package com.sda.vedingmachine.model;

/**
 * Enum with currency types that can be used by the Vending Machine.
 */
public enum CurrencyType {
    RON, EUR
}
