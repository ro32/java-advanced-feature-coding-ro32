package com.sda.vedingmachine.service;

import java.util.Scanner;

/**
 * Class used for input/output interactions.
 */
public class IoService {

    private Scanner scanner;

    public IoService() {
        this.scanner = new Scanner(System.in);
    }

    public void displayMessage(String message) {
        System.out.println(message);
    }

    public int readUserInput() {
        this.scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

}
