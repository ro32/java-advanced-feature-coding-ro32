package com.sda.person;

public class Developer extends Person{

    public Developer(){
        super();
        System.out.println("Developer");
    }

    public void nameForJavaDeveloper(){
        System.out.println("Name = " + this.getFirstname() + " " + this.getLastname());
    }

    protected void info() {
        System.out.println("Name = " + this.getFirstname() + "\n" +
                "Street = " + this.getStreet());
    }
}