package com.sda.personfile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Personal information system
 * a. Create a file containing any personal data (name, surname, phone number). Data of
 * individual persons should be in the following lines.
 * b. Download data from a file and create objects of people based on them (hint: String.split()).
 * c. Enter the created objects into ArrayList or Map (<line number>: <Person>).
 * d. Present the obtained data.
 **/
public class Main {

    public static final String COMMA = ",";
    public static List<Person> peopleList = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        readPersonalData().forEach(System.out::println);
    }

    private static List<Person> readPersonalData() throws IOException {
        Path path = Paths.get("src/main/resources/personalData.txt");
        List<String> fileLines = Files.readAllLines(path);

        for (String line : fileLines) {
            Person person = new Person();
            String[] data = line.split(COMMA);
            person.setName(data[0].trim());
            person.setSurname(data[1].trim());
            person.setPhoneNumber(data[2].trim());
            peopleList.add(person);
        }

        return peopleList;
    }
}
