package com.sda.onlineshop;

public interface Product {

    Double getPrice();

    String getName();

}
