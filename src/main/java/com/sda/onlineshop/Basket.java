package com.sda.onlineshop;

import java.util.ArrayList;
import java.util.List;

public class Basket {

    private final List<Product> products = new ArrayList<>();

    public void add(Product product) {
        products.add(product);
    }

    public void remove(Product product) {
        products.remove(product);
    }

    public List<Product> getProducts() {
        return new ArrayList<>(products); // we will return a copy of the initial list
    }

}
