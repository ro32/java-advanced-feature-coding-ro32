package com.sda.onlineshop;

public class OrderService {

    public Double calculatePrice(BasketImproved basket) {
        return basket.getProducts().entrySet().stream()
                .map(entry -> entry.getKey().getPrice() * entry.getValue())
                .reduce(0.0, Double::sum);
    }

}
