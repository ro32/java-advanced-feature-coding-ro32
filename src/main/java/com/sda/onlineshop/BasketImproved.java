package com.sda.onlineshop;

import java.util.HashMap;
import java.util.Map;

public class BasketImproved {

    private final Map<Product, Integer> products = new HashMap<>();

    public void add(Product product, Integer quantity) {
        if (products.containsKey(product)) {
            Integer existingQuantity = products.get(product);
            existingQuantity += quantity; // adunam cantitatea primita la cea existenta
            products.put(product, existingQuantity);

            // on AtomicInteger we have .getAndAdd(quantity) method
        } else {
            products.put(product, quantity);
            // on AtomicInteger it would become: new AtomicInteger(quatity) -> to transform the simple integer into atomic
        }
    }

    public void remove(Product product, Integer quantity) {
        if (products.containsKey(product) && products.get(product) <= quantity) { // on Atomic would have products.get(product).get() <= quantity
            products.remove(product); // stergem produsul de tot
        } else if (products.containsKey(product)) { // reducem cantitatea
            Integer qty = products.get(product);
            qty -= quantity;
            products.put(product, qty);
            // with Atomic: products.get(product).getAndAdd(-quantity);
        }
    }

    public Map<Product, Integer> getProducts() {
        return new HashMap<>(products); // we will return a copy of the keys
    }

}
