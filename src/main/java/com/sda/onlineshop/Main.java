package com.sda.onlineshop;

import java.util.List;

/**
 * Shop Application
 * <p>
 * Create a shop imitating application.
 * <p>
 * Create Basket class that will allow for adding, removing and retrieving all items in the basket.
 * <p>
 * Each item is an instance of Product interface that defines two methods:
 * ** one to get price of an item
 * ** and one to get name of an item
 * <p>
 * Create GenericProduct class that implements Product interface and will have:
 * ** supplier supplying price
 * ** supplier supplying name
 * <p>
 * Note: Make sure that Basket does not expose it’s internal list storing products (meaning it will not return
 * a reference to it’s internal list but rather a copy), all operations on that list should be done through Basket methods.
 * <p>
 * Add some products to the Basket.
 * <p>
 * Verify if everything works as expected.
 * <p>
 * Improve Basket so that it also tracks quantity of a particular product and allows for increment or decrement by a specified amount.
 * Hint AtomicInteger.
 * <p>
 * Create OrderService class that accepts instance of the Basket class and returns total price for all the items in the Basket that are available.
 * Use streams.
 */
public class Main {

    public static void main(String[] args) {
        Product milk = new GenericProduct(() -> 10.0, () -> "Milk");
        Product bread = new GenericProduct(() -> 1.0, () -> "Bread");
        Product oil = new GenericProduct(() -> 100.0, () -> "Olive Oil");

        Basket basket = new Basket();
        basket.add(milk);
        basket.add(bread);
        basket.add(oil);

        List<Product> products = basket.getProducts();
        System.out.println(products);

        products.remove(milk);
        System.out.println(products);
        System.out.println(basket.getProducts());

        basket.remove(milk);
        System.out.println(basket.getProducts());


        BasketImproved basketImproved = new BasketImproved();
        basketImproved.add(milk, 10);
        basketImproved.add(bread, 100);
        basketImproved.add(oil, 1);

        OrderService orderService = new OrderService();
        System.out.println(orderService.calculatePrice(basketImproved));
    }

}
