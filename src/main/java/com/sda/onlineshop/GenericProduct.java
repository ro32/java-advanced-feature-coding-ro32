package com.sda.onlineshop;

import java.util.function.Supplier;

public class GenericProduct implements Product {

    private final Supplier<Double> priceSupplier;
    private final Supplier<String> nameSupplier;

    public GenericProduct(Supplier<Double> priceSupplier, Supplier<String> nameSupplier) {
        this.priceSupplier = priceSupplier;
        this.nameSupplier = nameSupplier;
    }

    @Override
    public Double getPrice() {
        return priceSupplier.get();
    }

    @Override
    public String getName() {
        return nameSupplier.get();
    }

    @Override
    public String toString() {
        return "GenericProduct{" +
                "name=" + getName() +
                ", price=" + getPrice() +
                '}';
    }
}
