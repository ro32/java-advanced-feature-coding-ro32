package com.sda.storage;

import java.util.*;

public class Storage {

    private Map<String, List<String>> storage;

    public Storage() {
        this.storage = new HashMap<>();
    }

    public void addToStorage(String key, String value) {
        List<String> values;
        if (storage.containsKey(key)) {
            values = storage.get(key);
            values.add(value);
        } else {
            values = new ArrayList<>();
            values.add(value);
        }
        storage.put(key, values);
    }

    public void printValues(String key) {
        System.out.println(key + "->" + storage.get(key));
    }

    public List<String> findKeys(String value) {
        List<String> keys = new ArrayList<>();

        for (Map.Entry<String, List<String>> entry : storage.entrySet()) {
            if (entry.getValue().contains(value)) {
                keys.add(entry.getKey());
            }
        }
        return keys;
    }

    public void printStorage(){
        for (Map.Entry<String, List<String>> entry : storage.entrySet()) {
            System.out.println(entry.getKey() + "->" + entry.getValue());
        }
    }
}
