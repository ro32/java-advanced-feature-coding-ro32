package com.sda.storage;

/**
 * Create a Storage class that will have a private Map field, a public constructor, and methods:
 * The Storage class should allow you to store multiple values under one key.
 * *** addToStorage(String key, String value) → adding elements to the storage
 * *** printValues(String key) → displaying all elements under a given key
 * *** findValues(String value) → displaying all keys that have a given value
 */
public class Main {
    public static void main(String[] args) {

        Storage storage = new Storage();
        storage.addToStorage("Test", "masina");
        storage.addToStorage("Test", "fotbal");
        storage.addToStorage("Test3", "bere");
        storage.addToStorage("Test3", "masina");

        storage.printValues("Test");
        storage.printValues("Test3");
        System.out.println("masina -> " + storage.findKeys("masina"));
        System.out.println();
        storage.printStorage();

    }
}
