package com.sda.weapon;

import java.util.Stack;

public class WeaponMagazineStack {
    private final int weaponMagazineCapacity;
    private Stack<String> magazine;

    public WeaponMagazineStack(int weaponMagazineCapacity) {
        magazine = new Stack<>();
        this.weaponMagazineCapacity = weaponMagazineCapacity;
    }

    public void loadBullet(String bullet) {
        if (magazine.size() < weaponMagazineCapacity) {
            magazine.push(bullet);
        } else {
            System.out.println("full magazine");
        }
    }

    public boolean isLoaded() {
        return magazine.size() > 0;
    }

    public void shot() {
        if (isLoaded()) {
            System.out.println("The weapon fired the bullet " + magazine.pop());
        } else {
            System.out.println("empty magazine");
        }
    }
}
