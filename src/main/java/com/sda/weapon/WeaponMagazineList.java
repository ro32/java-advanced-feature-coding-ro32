package com.sda.weapon;

import java.util.ArrayList;
import java.util.List;

public class WeaponMagazineList {

    private int size;
    private List<String> magazines;

    public WeaponMagazineList(int size) {
        this.size = size;
        this.magazines = new ArrayList<>();
    }

    public void loadBullet(String bullet) {
        if (magazines.size() < size) {
            this.magazines.add(bullet);
        } else {
            System.out.println("The magazine is overloaded");
        }
    }

    public boolean isLoaded() {
//        if (this.magazines.size() >0){
//            return true;
//        }else{
//            return false;
//        }
        return this.magazines.size() > 0;
    }

    public void shot() {
        if (this.isLoaded()) {
            System.out.println("Take care, the magazine has bullets");
            System.out.println("Shot -> " + magazines.get(this.magazines.size() - 1));
            magazines.remove(this.magazines.size() - 1);
        } else {
            System.out.println("Empty magazine; no bullets found");
        }
    }
}
