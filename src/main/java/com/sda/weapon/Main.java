package com.sda.weapon;

/**
 * Create a class imitating a weapon magazine.
 * The class should be able to define the size of the magazine using the constructor.
 * <p>
 * Implement the methods:
 * loadBullet(String bullet) → adding a cartridge to the magazine, does not allow loading more cartridges
 * than the capacity of the magazine
 * <p>
 * isLoaded() → returns information about whether the weapon is loaded (at least one cartridge) or not
 * <p>
 * shot() → each call shots one bullet (prints string value in console) - the last loaded cartridge - and
 * prepares the next one, loaded before the last one, if there are no more cartridges, it prints "empty
 * magazine" in the console
 */
public class Main {
    public static void main(String[] args) {

        System.out.println("-------------- Weapon Magazine Using List ----------------");
        WeaponMagazineList weaponMagazine = new WeaponMagazineList(5);
        weaponMagazine.loadBullet("bullet1");
        weaponMagazine.loadBullet("bullet2");
        weaponMagazine.loadBullet("bullet3");
        weaponMagazine.loadBullet("bullet4");
        weaponMagazine.loadBullet("bullet5");
        weaponMagazine.loadBullet("bullet6");

        for (int i = 0; i < 8; i++) {
            weaponMagazine.shot();
        }

        System.out.println("-------------- Weapon Magazine Using Stack ----------------");

        WeaponMagazineStack weapon = new WeaponMagazineStack(5);
        weapon.loadBullet("bullet1");
        weapon.loadBullet("bullet2");
        weapon.loadBullet("bullet3");
        weapon.loadBullet("bullet4");
        weapon.loadBullet("bullet5");
        weapon.loadBullet("bullet6");

        for (int i = 0; i < 8; i++) {
            weapon.shot();
        }

    }
}
