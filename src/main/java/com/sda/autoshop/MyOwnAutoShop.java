package com.sda.autoshop;

/**
 * Ex 3
 * Create a super class called Car. The Car class has the following fields and methods.
 * int speed;
 * double regularPrice;
 * String color;
 * double getSalePrice();
 * Create a subclass of Car class and name it as Truck. The Truck class has the following fields and methods.
 * int weight;
 * double getSalePrice();   //Ifweight>2000,10%discount.
 * <p>
 * Create a subclass of Car class and name it as Ford. The Ford class has the following fields and methods
 * int year;
 * int manufacturerDiscount;
 * double getSalePrice(); // From the sale price computed from Car class, subtract the manufacturer Discount.
 * Create a subclass of Car class and name it as Sedan. The Sedan class has the following fields and methods.
 * int length;
 * double getSalePrice(); // Ilength>20feet, 5%discount, otherwise, 10%discount
 * <p>
 * Create MyOwnAutoShop class which contains the main() method. Perform the following within the main() method.
 * Create an instance of Sedan class and initialize all the fields with appropriate values. Use super(...) method in the constructor for initializing the fields of the superclass.
 * Create two instances of the Ford class and initialize all the fields with appropriate values. Use super(...) method in the constructor for initializing the fields of the super class
 * Create an instance of Car class and initialize all the fields with appropriate values. Display the sale prices of all instance.
 */
public class MyOwnAutoShop {
    public static void main(String[] args) {
        Sedan sedan = new Sedan(200, 10000.0, "red", 20);
        Ford ford1 = new Ford(205, 15000.0, "blue", 2020, 20);
        Ford ford2 = new Ford(210, 20000.0, "green", 2021, 15);
        Car car = new Car(150, 9000.0, "white");
        System.out.println("Final sale price for sedan is " + sedan.getRegularPrice());
        System.out.println("Final sale price for ford1 is " + ford1.getRegularPrice());
        System.out.println("Final sale price for ford2 is " + ford2.getRegularPrice());
        System.out.println("Final sale price for car is " + car.getRegularPrice());

        Sedan sedan1 = new Sedan(150, 5000.0, "black", 18);
        System.out.println(sedan1.getRegularPrice());

        Ford carFord = new Ford(200, 10000.0, "blue", 2021, 10);
        System.out.println(carFord.getRegularPrice());

        Truck truck = new Truck(150, 25000.0, "white", 2020);
        System.out.println(truck.getRegularPrice());

        Car car1 = new Car(150, 2500.0, "brown");
        System.out.println(car1.getRegularPrice());
    }
}
