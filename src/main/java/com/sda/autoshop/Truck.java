package com.sda.autoshop;

public class Truck extends Car {
    private int weight;

    public Truck(int speed, double regularPrice, String color, int weight) {
        super(speed, regularPrice, color);
        this.weight = weight;
    }

    @Override
    public double getRegularPrice() {
        double price;
        if (weight > 2000) {
            price = getRegularPrice() - (10.00 / 100 * getRegularPrice());
        } else {
            price = getRegularPrice();
        }
        return price;

    }

}
