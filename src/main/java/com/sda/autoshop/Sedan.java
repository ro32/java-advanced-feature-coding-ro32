package com.sda.autoshop;

public class Sedan extends Car {
    private int length;

    public Sedan(int speed, double regularPrice, String color, int length) {
        super(speed, regularPrice, color);
        this.length = length;
    }

    @Override
    public double getRegularPrice() {
        double salePrice;
        if (length > 20) {
            salePrice = regularPrice - (5.0 / 100 * regularPrice);
        } else {
            salePrice = regularPrice - (10.0 / 100 * regularPrice);
        }
        return salePrice;
    }

    public double getRegularPriceV1() {
        return length > 20 ? regularPrice - regularPrice * (5 / 100.0) :
                regularPrice - regularPrice * (10.0 / 100);
    }

}
